import Vue from 'vue'
import Vuetify from 'vuetify'
import App from './App'
import * as firebase from 'firebase'
import router from './router'
import { store } from './store'
import DateFilter from './filters/date'
import AlertCmp from './components/share/alert.vue'
import EditNewsItemDialog from './components/News/Edit/EditNewsItemDialog.vue'
import EditNewsItemDateDialog from './components/News/Edit/EditNewsItemDateDialog.vue'
import EditNewsItemTimeDialog from './components/News/Edit/EditNewsItemTimeDialog.vue'

Vue.use(Vuetify)
Vue.config.productionTip = false

Vue.filter('date', DateFilter)
Vue.component('app-alert', AlertCmp)
Vue.component('app-edit-newsItem-dialog', EditNewsItemDialog)
Vue.component('app-edit-newsItem-date-dialog', EditNewsItemDateDialog)
Vue.component('app-edit-newsItem-time-dialog', EditNewsItemTimeDialog)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
  created () {
    firebase.initializeApp({
      apiKey: "AIzaSyBM-kCwqtCOSfDhdllqmt9bhAnoao4Pe6g",
      authDomain: "bootfi-news.firebaseapp.com",
      databaseURL: "https://bootfi-news.firebaseio.com",
      projectId: "bootfi-news",
      storageBucket: "bootfi-news.appspot.com",
      messagingSenderId: "279935701895",
      appId: "1:279935701895:web:632d6587e92d5777c7b922",
      measurementId: "G-BWPJ7PY1TJ"
    })
    firebase.database().settings({ timestampsInSnapshots: true})
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.$store.dispatch('autoSignIn', user)
      }
    })
    this.$store.dispatch('loadNewsItems')
  }

})
