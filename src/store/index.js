import vue from 'vue'
import vuex from 'vuex'

import newsItem from './NewsItem'
import user from './User'
import share from './share'

vue.use(vuex)

export const store = new vuex.Store({
  modules: {
    newsItem: newsItem,
    user: user,
    share: share
  }
})
