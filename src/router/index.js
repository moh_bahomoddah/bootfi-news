import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import NewsItems from '@/components/News/NewsItems'
import NewsItem from '@/components/News/NewsItem'
import CreateNewsItem from '@/components/News/CreateNewsItem'
import Profile from '@/components/Users/Profile'
import Signup from '@/components/Users/Signup'
import Signin from '@/components/Users/Signin'
import AuthGuard from './auth-guard'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/newsitems',
      name: 'NewsItems',
      component: NewsItems
    },
    {
      path: '/newsitems/new',
      name: 'CreateNewsItem',
      component: CreateNewsItem,
      beforeEnter: AuthGuard
    },
    {
      path: '/newsitems/:id',
      name: 'NewsItem',
      props: true,
      component: NewsItem
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile,
      beforeEnter: AuthGuard
    },
    {
      path: '/signup',
      name: 'Signup',
      component: Signup
    },
    {
      path: '/signin',
      name: 'Signin',
      component: Signin
    }
  ]
})
